
# (SINGLE-SOURCE) SHORTEST PATH

# Ergaenzung von dijkstra um Pfade

def dijkstra_paths(G,s):
    m = len(G)
    d = [None]*m                # (geschaetzte) Pfadkosten
    d[s] = 0                    # sind 0 fuer den Startknoten
    p = [None]*m                # Vorgänger-Knoten Liste
    Q = {u for u in range(m)}   # alle Knoten
    while Q:
        # Auswahl von v mit kleinstem d-Wert
        (_,v) = min({(d[u],u) for u in Q if d[u]!= None})
        # v aus Q entfernen, d[v] ist endgueltig
        Q.remove(v)
        # Schaetzungen fuer Nachfolger von v aktualisieren
        for u in G[v]:
            alt = d[v] + G[v][u]            # alternative Kosten
            if d[u]==None or alt < d[u]:
                d[u] = alt
                p[u]=v
    return d, p

"""
# s-v-Pfad bestimmen
def shortest_path(s,v,p):
    pu = [v]
    while p[v] !=s and p[v]!=None:
        v=p[v]
        pu.insert(0,v)
    if s!=v:
        pu.insert(0,s)
    return pu
"""
def shortest_path(s,v,p):
    pu=[]
    if p[v] !=s and p[v]!=None:
        v=p[v]
        pu.extend(shortest_path(s,v,p))
        pu.append(v)
    else:
        pu.append(s)
    return pu

# Bsp aus der VL
# Knotennummern: s=0, u=1, x=2, y=3, v=4, z=5
def define_G():
    G = [   {1:1, 4:4, 2:2},
            {3:3, 4:1},
            {4:2, 5:3},
            {},
            {3:1, 5:2},
            {}
        ]
    return G

G = define_G()
d,p = dijkstra_paths(G,0)
print(d,p)
print(shortest_path(0,3,p))
print(shortest_path(0,0,p))

