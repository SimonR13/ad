from itertools import product# MAX KNAPSACK

# zulaessige Loesung, auch fuer Teilloesungen
def sol_max_ks(S,t):
    if t <= S:
        return True
    return False

# Bewertungsfunktion, auch fuer Teilloesungen
def m_max_ks(h,opt):
    if h>opt:
        return h
    return opt

# Entwurfsmuster Exhaustive Search
from itertools import product
def max_ks_exhaustive(s,v,S):            
    opt = -1
    for i in product(range(2),repeat=len(s)):
        t=0
        h=0
        for j in range(len(i)):
            if i[j]==1:
                t=t+s[j]
                h=h+v[j] 
        if sol_max_ks(S,t):
            opt=m_max_ks(h,opt)
    return opt


# Bsp m=4, S=7
print(max_ks_exhaustive([3,2,1,5],[3,1,1,4],7))
print(max_ks_exhaustive([3,2,1,5],[3,1,1,4],8))
print(max_ks_exhaustive([3, 9, 10, 10, 1, 9, 10, 8, 10, 4],[3, 5, 7, 1, 2, 6, 10, 1, 7, 4],20))



